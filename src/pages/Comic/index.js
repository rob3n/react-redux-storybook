import React, { Component } from 'react';
import styled from 'styled-components';
import { getLastPath } from '../../utils';
import {
  CardMedia,
  Grid,
  Typography,
} from '../../../node_modules/@material-ui/core';

const StyledCardMedia = styled(CardMedia)`
  && {
    margin-top: 50px;
    height: 450px;
    background-size: contain;
  }
`;

const StyledTypography = styled(Typography)`
  && {
    padding: 50px 100px 0;
  }
`;

class Comic extends Component {
  static defaultProps = {};

  state = {};

  async componentDidMount() {
    await this.getComic();
  }

  getComic = () => {
    // example own Promise
    // function getLastPath() {
    //   const windowPath = window.location.url;
    //   const name = windowPath.split('/');

    //   return name[name.length - 1];
    // }
    // fetch example

    const id = getLastPath(this.props.match);

    if (id) {
      fetch(
        `https://gateway.marvel.com/v1/public/comics/${id}?ts=1&apikey=12077ea809e2bf0e7f6571f01e2e0765&hash=049552d6a6574a0517ece0ecf2c3cfb8`,
      )
        .then(response => response.json())
        .then(data => this.setState({ comic: data.data.results[0] }))
        .catch(err => console.log(err));
    }
  };

  render() {
    const { comic } = this.state;
    return (
      <Grid container>
        <Grid xs={4}>
          {comic &&
            comic.thumbnail && (
              <StyledCardMedia
                image={`${comic.thumbnail.path}.${comic.thumbnail.extension}`}
              />
            )}
        </Grid>
        <Grid xs={8}>
          {comic &&
            comic.description && (
              <StyledTypography>{comic.description}</StyledTypography>
            )}
        </Grid>
      </Grid>
    );
  }
}

export default Comic;

Comic.propTypes = {};
