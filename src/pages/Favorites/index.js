import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Typography } from '../../../node_modules/@material-ui/core';
import Content from './../Main/components/Content';

const StyledTitle = styled(Typography)`
  && {
    margin-top: 50px;
    text-align: center;
    font-size: 42px;
    text-transform: uppercase;
  }
`;

class Favorites extends React.Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <StyledTitle variant="title">Favorites</StyledTitle>
        <Content data={this.props.favorites} />
      </React.Fragment>
    );
  }
}

export default connect(state => ({
  favorites: state.favorites,
}))(Favorites);
