import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Grid, Typography } from '@material-ui/core';
import Content from './components/Content';
// import Sidebar from './components/Sidebar';
import Search from '../../components/Search';
import { getData } from '../../utils';
import { ShieldIcon } from '../../svg/icons';
import { addToFavoriteAction, deleteFromFavoriteAction } from '../../actions';

const PageWrap = styled.div`
  position: relative;
  height: 100%;
`;

const IconContainer = styled(Grid)`
  height: 100%;
  svg {
    margin-top: 200px;
  }
`;

class Main extends Component {
  state = {
    query: '',
    openSearchResults: false,
    onSelect: false,
    preloader: false,
  };

  componentDidMount() {
    this.getCommonResultsData();
    document.addEventListener('keydown', this.submit, false);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.submit, false);
  }

  getCommonResultsData = async () => {
    this.setState({ preloader: true });
    const getContent = await getData('/comics');

    if (getContent) {
      this.setState({ comics: getContent.data.data.results, preloader: false });
    }
  };

  addToFavorite = () => {
    console.log(this.props);
    this.props.addToFavoriteAction(Math.floor(Math.random() * 10));
  };

  getResultsData = async value => {
    // example own Promise

    this.setState({ preloader: true });

    function delay(time) {
      return new Promise((resolve, rejects) => {
        if (isNaN(time)) {
          rejects(new Error('its not a number'));
        }
        setTimeout(resolve, time);
      });
    }

    delay(3000)
      .then(() => console.log('right way'))
      .catch(err => console.error(err));

    // fetch example with try cath and await

    const getDataWithFetch = await fetch(
      `https://gateway.marvel.com:443/v1/public/comics?title=${value}&ts=1&apikey=12077ea809e2bf0e7f6571f01e2e0765&hash=049552d6a6574a0517ece0ecf2c3cfb8`,
    );
    const getDataWithHulk = await fetch(
      `https://gateway.marvel.com:443/v1/public/comics?title=Hulk&ts=1&apikey=12077ea809e2bf0e7f6571f01e2e0765&hash=049552d6a6574a0517ece0ecf2c3cfb8`,
    );

    const getDataJson = await getDataWithFetch.json();
    const getDataJsonHulk = await getDataWithHulk.json();

    const promises = [getDataJson, getDataJsonHulk];

    // fetch all promises

    Promise.all(promises).then(results => {
      const comicsArr = [];
      for (let i = 0; i < results.length; i++) {
        for (let j = 0; j < results[i].data.results.length; j++) {
          comicsArr.push(results[i].data.results[j]);
        }
      }

      this.setState({ comics: comicsArr, preloader: false });
    });
  };

  deleteFromFavorite = id => {
    // console.log(id);
    this.props.deleteFromFavoriteAction(id);
  };

  submit = e => {
    if (e.keyCode === 13 && !this.state.onSelect) {
      this.getResultsData(this.state.query);
    }
  };

  handleChange = e => {
    if (e.target.value) {
      this.setState({ openSearchResults: true });
    } else {
      this.setState({ openSearchResults: false });
    }
    this.setState({ query: e.target.value });
  };

  handleSelect = value => {
    this.setState({ query: value, openSearchResults: false, onSelect: true });
    this.getResultsData(value);
  };

  clearSearch = () => {
    this.setState({ query: '' });
  };

  render() {
    const { preloader } = this.state;

    return (
      <PageWrap>
        <Search
          open={this.state.openSearchResults}
          onChange={this.handleChange}
          handleSelect={this.handleSelect}
          query={this.state.query}
          clearSearch={this.clearSearch}
        />
        {preloader ? (
          <IconContainer
            container
            justify="center"
            alignItems="center"
            direction="column"
          >
            <ShieldIcon />
            <Typography
              variant="paragraph"
              style={{
                color: '#ef403c',
                marginTop: '40px',
                textTransform: 'uppercase',
              }}
            >
              Loading...
            </Typography>
          </IconContainer>
        ) : (
          <React.Fragment>
            <Content data={this.state.comics} />
          </React.Fragment>
        )}
      </PageWrap>
    );
  }
}

export default connect(
  state => ({
    favorites: state.favorites,
  }),
  { addToFavoriteAction, deleteFromFavoriteAction },
)(Main);
