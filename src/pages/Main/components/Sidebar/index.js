import React, { Component } from 'react';
import { Drawer } from '@material-ui/core';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { toggleDrawerAction } from '../../../../actions';

const StyledDrawer = styled(Drawer)`
  && {
  }
`;

const StyledForm = styled.form`
  width: 300px;
`;

class Sidebar extends Component {
  handleToggle = () => {
    console.log('close');
    const { toggleDrawerAction } = this.props;

    toggleDrawerAction();
  };

  render() {
    const { isOpen } = this.props;

    return (
      <React.Fragment>
        <StyledDrawer open={isOpen} onClose={this.handleToggle} anchor="right">
          <StyledForm />
        </StyledDrawer>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    isOpen: state.isOpen,
  }),
  { toggleDrawerAction },
)(Sidebar);
