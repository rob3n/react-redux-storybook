import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Card, CardMedia, CardContent, Typography } from '@material-ui/core';

const StyledCardMedia = styled(CardMedia)`
  && {
    height: 300px;
    margin-top: 1rem;
    background-size: contain;
  }
`;

const StyledCard = styled(Card)`
  && {
    height: 100%;
    transition: 0.3s all ease-in-out;
    cursor: pointer;
    :hover {
      box-shadow: 5px 15px 30px 0px rgba(0, 0, 0, 0.2),
        0px 10px 2px 0px rgba(0, 0, 0, 0.14),
        0px 3px 1px -2px rgba(0, 0, 0, 0.12);
    }
  }
`;

class Comic extends Component {
  static defaultProps = {
    data: {},
  };

  state = {};

  render() {
    const { data } = this.props;

    return (
      <StyledCard>
        <StyledCardMedia
          image={`${data.thumbnail.path}.${data.thumbnail.extension}`}
          title={data.title}
        />
        <CardContent>
          {data.title && <Typography variant="title">{data.title}</Typography>}
        </CardContent>
      </StyledCard>
    );
  }
}

export default Comic;

Comic.propTypes = {
  data: PropTypes.shape({}),
};
