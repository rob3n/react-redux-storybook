import React, { Component } from 'react';
import styled from 'styled-components';
import { Card, CardMedia, CardContent, Typography } from '@material-ui/core';

const StyledCardMedia = styled(CardMedia)`
  && {
    height: 300px;
  }
`;

const StyledCard = styled(Card)`
  max-height: 500px;
`;

class Character extends Component {
  static defaultProps = {};

  state = {};

  render() {
    const { data } = this.props;

    return (
      <StyledCard>
        <StyledCardMedia
          image={`${data.thumbnail.path}.${data.thumbnail.extension}`}
          title={data.title}
        />
        <CardContent>
          <Typography>{data.description}</Typography>
        </CardContent>
      </StyledCard>
    );
  }
}

export default Character;

Character.propTypes = {};
