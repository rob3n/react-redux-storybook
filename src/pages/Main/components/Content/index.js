import React, { Component } from 'react';
import { Grid, Typography } from '@material-ui/core';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Comic from './Comic';
import {
  toggleDrawerAction,
  addToFavoriteAction,
  deleteFromFavoriteAction,
} from '../../../../actions';
import { compare } from '../../../../utils';
// import { getCharacters } from '../../../../utils';

const Wrap = styled.div`
  max-width: 1440px;
  margin: 0 auto;
`;

const List = styled(Grid)`
  && {
    list-style: none;
    padding: 2rem 2rem 6rem;
  }
`;

const ListItem = styled(Grid)`
  && {
    margin-bottom: 3rem !important;
  }
  a {
    text-decoration: none;
    color: inherit;
  }
`;

const FavoriteControl = styled(Typography)`
  && {
    text-align: center;
    border-radius: 3px 3px 0 0;
    transition: 0.2s all ease-in-out;
    cursor: pointer;
    text-transform: uppercase;
    padding: 0.5rem 0;
    border: 1px solid transparent;
    ${p =>
      p.addToFavorite
        ? `background: #f0141e;
           color: #fff;
          `
        : `border: 1px solid #f0141e;
           color: #f0141e;
           :hover {
            background: #f0141e;
            color: #fff;
           }`};
  }
`;

class Content extends Component {
  state = {};

  toggleHandler = () => {
    const { toggleDrawerAction } = this.props;
    toggleDrawerAction();
  };

  addToFavorite = comic => {
    this.props.addToFavoriteAction(comic);
  };

  deleteFromFavorite = comic => {
    this.props.deleteFromFavoriteAction(comic);
  };

  render() {
    const { data, favorites } = this.props;

    return (
      <Wrap>
        <List container spacing={24} alignItems="stretch">
          {data &&
            data.map(
              comic =>
                comic.description &&
                comic.images.length > 0 && (
                  <React.Fragment>
                    <ListItem key={comic.id} item xs={3}>
                      {compare(data, favorites).includes(comic) ? (
                        <FavoriteControl
                          variant="body2"
                          onClick={() => this.deleteFromFavorite(comic)}
                          addToFavorite={compare(data, favorites).includes(
                            comic,
                          )}
                        >
                          Remove from favorites
                        </FavoriteControl>
                      ) : (
                        <FavoriteControl
                          variant="body2"
                          onClick={() => this.addToFavorite(comic)}
                        >
                          Add to favorites
                        </FavoriteControl>
                      )}
                      <Link
                        href={`/comic/${comic.id}`}
                        to={`/comic/${comic.id}`}
                      >
                        <Comic data={comic} />
                      </Link>
                    </ListItem>
                  </React.Fragment>
                ),
            )}
        </List>
      </Wrap>
    );
  }
}

export default connect(
  state => ({
    isOpen: state.isOpen,
    favorites: state.favorites,
  }),
  { toggleDrawerAction, addToFavoriteAction, deleteFromFavoriteAction },
)(Content);
