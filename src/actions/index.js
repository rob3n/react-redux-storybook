import {
  INCREMENT,
  DELETE_ARTICLE,
  ADD_TO_FAVORITE,
  DELETE_FROM_FAVORITE,
} from '../constants';

export const increment = () => {
  return {
    type: INCREMENT,
  };
};

export const deleteArticle = id => {
  return {
    type: DELETE_ARTICLE,
    payload: { id },
  };
};

export const toggleDrawerAction = () => {
  return {
    type: 'TOGGLE_DRAWER',
  };
};

export const addToFavoriteAction = data => {
  return {
    type: ADD_TO_FAVORITE,
    payload: { data },
  };
};

export const deleteFromFavoriteAction = data => {
  return {
    type: DELETE_FROM_FAVORITE,
    payload: { data },
  };
};
