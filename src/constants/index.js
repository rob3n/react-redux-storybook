export const DELETE_ARTICLE = 'DELETE_ARTICLE';

export const INCREMENT = 'INCREMENT';
export const ADD_TO_FAVORITE = 'ADD_TO_FAVORITE';
export const DELETE_FROM_FAVORITE = 'DELETE_FROM_FAVORITE';
