import axios from 'axios';

export const getData = async url => {
  const params = {
    method: 'get',
    url: `https://gateway.marvel.com:443/v1/public${url}?ts=1&apikey=12077ea809e2bf0e7f6571f01e2e0765&hash=049552d6a6574a0517ece0ecf2c3cfb8`,
  };

  return axios(params);
};

export const getCharacters = async url => {
  const params = {
    method: 'get',
    url: `https://gateway.marvel.com:443/v1/public${url}?limit=100&ts=1&apikey=12077ea809e2bf0e7f6571f01e2e0765&hash=049552d6a6574a0517ece0ecf2c3cfb8`,
  };

  return axios(params);
};

export const getDataWithName = async url => {
  const params = {
    method: 'get',
    url: `https://gateway.marvel.com:443/v1/public${url}&ts=1&apikey=12077ea809e2bf0e7f6571f01e2e0765&hash=049552d6a6574a0517ece0ecf2c3cfb8`,
  };

  return axios(params);
};

export const truncate = (str, maxlength) => {
  return str.length > maxlength
    ? `${str
        .split(' ')
        .splice(0, maxlength - 3)
        .join(' ')}...`
    : str;
};

export const getLastPath = location => {
  const windowPath = location.url;
  const name = windowPath.split('/');

  return name[name.length - 1];
};

export const compare = (arr1, arr2) => {
  const resultArray = [];

  arr1.forEach(element => {
    arr2.forEach(element2 => {
      if (element.id === element2.id) {
        resultArray.push(element);
      }
    });
  });

  return resultArray;
};
