import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { increment } from '../actions';

class Counter extends Component {
  static defaultProps = {
    counter: 1,
  };

  state = {};

  handleIncrement = () => {
    this.props.increment();
    console.log(this.props);
  };

  render() {
    return (
      <div>
        <h2>{this.props.counter}</h2>
        <Button onClick={this.handleIncrement}>Increment</Button>
      </div>
    );
  }
}

// const MapStateToProps = state => {
//   return {
//     counter: state.count,
//   };
// };

// const MapToDispatch = { increment };

// const decorator = connect(
//   MapStateToProps,
//   MapToDispatch,
// );

export default connect(
  state => ({
    counter: state.count,
  }),
  { increment },
)(Counter);

Counter.propTypes = {
  counter: PropTypes.number,
  increment: PropTypes.func.isRequired,
};
