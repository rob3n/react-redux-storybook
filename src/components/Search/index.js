import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import styled from 'styled-components';
import Autocomplete from 'react-autocomplete';
import { articles } from '../../data';

// const StyledField = styled(TextField)`
//   && {
//     width: 50%;
//     margin: 2rem 0;
//   }
// `;

const Wrap = styled(Grid)`
  margin-top: 2rem;
  z-index: 100;
  position: relative;
  input:focus {
    outline: none;
  }
  .item-highlighted {
    font-size: 150%;
    transition: 0.2s all ease-in-out;
  }
`;

const CloseIcon = styled.div`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  width: 15px;
  height: 15px;
  cursor: pointer;
  :after {
    content: '';
    width: 100%;
    height: 1px;
    background: #000;
    transform: rotate(45deg);
    position: absolute;
    left: 0;
    top: 0;
  }
  :before {
    content: '';
    width: 100%;
    height: 1px;
    background: #000;
    transform: rotate(-45deg);
    position: absolute;
    left: 0;
    top: 0;
  }
`;

export function matchStateToTerm(state, value) {
  return (
    state.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
    state.toLowerCase().indexOf(value.toLowerCase()) !== -1
  );
}

export function sortStates(a, b, value) {
  const aLower = a.toLowerCase();
  const bLower = b.toLowerCase();
  const valueLower = value.toLowerCase();
  const queryPosA = aLower.indexOf(valueLower);
  const queryPosB = bLower.indexOf(valueLower);
  if (queryPosA !== queryPosB) {
    return queryPosA - queryPosB;
  }
  return aLower < bLower ? -1 : 1;
}

class Search extends Component {
  state = {};

  render() {
    const { onChange, query, handleSelect, open, clearSearch } = this.props;

    return (
      <Wrap container justify="center">
        <Grid item xs={4} style={{ position: 'relative' }}>
          <Autocomplete
            getItemValue={item => item}
            items={articles}
            value={query}
            onChange={onChange}
            onSelect={e => handleSelect(e)}
            open={open}
            inputProps={{
              autoFocus: true,
              style: {
                border: 'none',
                borderBottom: '1px solid',
                width: '100%',
                fontSize: '1.2rem',
                transition: '1s all ease-in-out',
              },
            }}
            shouldItemRender={matchStateToTerm}
            wrapperStyle={{
              position: 'relative',
              display: 'inline-block',
              width: '100%',
            }}
            sortItems={sortStates}
            renderItem={(item, isHighlighted) => (
              <div
                className={`item ${isHighlighted ? 'item-highlighted' : ''}`}
                key={item}
              >
                {item}
              </div>
            )}
          />
          {query && <CloseIcon onClick={clearSearch} />}
        </Grid>
      </Wrap>
    );
  }
}

export default Search;

Search.propTypes = {};
