import React, { Component } from 'react';
import { AppBar, Grid, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { FavoritesIcon } from '../../svg/icons';

const StyledAppBar = styled(AppBar)`
  padding: 1rem 1rem;
  position: relative;
  && {
    position: static;
    background-color: #000;
  }
  a {
    text-decoration: none;
  }
`;

const HeaderLogo = styled(Typography)`
  && {
    color: #f0141e;
    text-transform: uppercase;
    font-weight: bold;
    font-family: Bebas;
    transition: 0.3s all ease-in;
    :hover {
      color: #fff;
    }
  }
`;

const HeaderLink = styled(Link)`
  text-decoration: none;
`;

const IconFavorite = styled(FavoritesIcon)`
  position: absolute;
  cursor: pointer;
`;

class Header extends Component {
  static defaultProps = {};

  state = {};

  render() {
    const { favorites } = this.props;
    return (
      <StyledAppBar>
        <Grid container justify="center">
          <HeaderLink to="/">
            <HeaderLogo variant="display3">Marvel</HeaderLogo>
          </HeaderLink>
          <Link to="/favorites" href="/favorites">
            <IconFavorite
              counter={favorites.length > 0 && favorites.length}
              notEmpty={favorites.length > 0}
            />
          </Link>
        </Grid>
      </StyledAppBar>
    );
  }
}

export default connect(state => ({
  favorites: state.favorites,
}))(Header);

Header.propTypes = {};
