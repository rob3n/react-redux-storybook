import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Item from './Item';

class List extends PureComponent {
  state = {};

  render() {
    const { articles } = this.props;

    const renderList =
      articles &&
      articles.map(article => <Item key={article.id} article={article} />);

    return <React.Fragment>{renderList}</React.Fragment>;
  }
}

export default connect(state => ({
  articles: state.articles,
}))(List);
