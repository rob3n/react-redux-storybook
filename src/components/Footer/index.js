import React, { Component } from 'react';
import styled from 'styled-components';

const StyledFooter = styled.footer`
  background-color: #000;
  padding: 2rem 1rem;
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
`;

class Footer extends Component {
  static defaultProps = {};

  state = {};

  render() {
    return <StyledFooter />;
  }
}

export default Footer;

Footer.propTypes = {};
