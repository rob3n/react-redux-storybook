import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteArticle } from '../actions';

class Item extends Component {
  state = {};

  handleDelete = () => {
    const { deleteArticle, article } = this.props;
    deleteArticle(article.id);
  };

  render() {
    const { article } = this.props;
    return (
      <li>
        {article.title} <button onClick={this.handleDelete}>Delete</button>
      </li>
    );
  }
}

export default connect(
  null,
  { deleteArticle },
)(Item);
