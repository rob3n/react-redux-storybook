import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from './../reducers';

const store = createStore(reducer, composeWithDevTools());

// for dev
window.store = store;

export default store;
