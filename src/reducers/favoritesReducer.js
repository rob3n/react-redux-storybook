import { ADD_TO_FAVORITE, DELETE_FROM_FAVORITE } from '../constants';

export default (favoritesState = [], action) => {
  const { type, payload } = action;

  switch (type) {
    case DELETE_FROM_FAVORITE:
      return favoritesState.filter(
        currentItem => currentItem.id !== payload.data.id,
      );
    case ADD_TO_FAVORITE:
      for (let i = 0; i < favoritesState.length; i++) {
        if (favoritesState[i].id === payload.data.id) {
          return favoritesState;
        }
      }
      return favoritesState.concat(payload.data);
    default:
      return favoritesState;
  }
};
