import { combineReducers } from 'redux';
import counterReducer from './counter';
import articlesReducer from './articlesReducer';
import drawerToggleReducer from './drawerReducer';
import addToFavorite from './favoritesReducer';

export default combineReducers({
  count: counterReducer,
  articles: articlesReducer,
  isOpen: drawerToggleReducer,
  favorites: addToFavorite,
});
