export default (state = false, action) => {
  const { type } = action;

  switch (type) {
    case 'TOGGLE_DRAWER':
      console.log('toggle from reducer');
      return !state;
    default:
      return state;
  }
};
