import React from 'react';
import { Provider } from 'react-redux';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { indigo, purple } from '@material-ui/core/colors';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import './App.css';
import Main from './pages/Main';
import Header from './components/Header';
import store from './store';
import Comic from './pages/Comic';
import Favorites from './pages/Favorites';

const theme = createMuiTheme({
  palette: {
    primary: purple,
    secondary: indigo,
  },
});

const App = () => {
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
          <React.Fragment>
            <Header />
            <Switch>
              <Route path="/" exact component={Main} />
              <Route path="/comic/:id" exact component={Comic} />
              <Route path="/favorites" exact component={Favorites} />
            </Switch>
          </React.Fragment>
        </BrowserRouter>
      </MuiThemeProvider>
    </Provider>
  );
};

export default App;
